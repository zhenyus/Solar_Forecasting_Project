from __future__ import print_function
import os
import platform
import json
import sys
from wu_history_file import *

class wu_hist(object):
	def __init__(self, name):
		if platform.system() == 'Windows':
			self.split = "\\"
		else:
			self.split = "/"
		self.path_wu_history = "..%sdata%swu_history%s"%(self.split, self.split,self.split)
		self.name = name
		self.header = ["TimePST", "TemperatureF", "Dew PointF", "Humidity", "Sea Level PressureIn", "VisibilityMPH",
					 "Wind Direction","Wind SpeedMPH", "Gust SpeedMPH", "PrecipitationIn",
					 "Events", "Conditions", "WindDirDegrees","DateUTC"]

#	def access_history(self):
#		folder = self.name
#		try:
#			os.stat(self.path_wu_history + folder)
#		except Exception as e:
#			print("[Warn] Cannot locate the folder. Create a new one instead.")
#			os.mkdir(self.path_wu_history + folder)
#		list_output = []
#		list_file = os.listdir(self.path_wu_history + folder)
#		for filename in list_file:
#			if platform.system() == 'Windows':
#				fp = open(self.path_wu_history + folder + '\\' + filename)
#			else:
#				fp = open(self.path_wu_history + folder + '/' + filename)
#			content = fp.readlines()
#			fp.close()
#			print(content.pop(0))
#			content.pop(0)
#			list_output.append([i.split(',') for i in content])
#		return list_output

	def access_file(self, filename):
		try:
			os.stat(self.path_wu_history + self.name)
		except Exception as e:
			print("[Warn] Cannot locate the folder. Create a new one instead.")
			os.mkdir(self.path_wu_history + self.name)
		with open(self.path_wu_history + self.name + self.split + filename) as f:
			content = f.readlines()
		return self.parse(content)
				
	def parse(self, content):
		content.pop(0)	#blank row
		header = content.pop(0).replace("<br />", "").strip().split(',')  #header row
		data = [i.replace("<br />", "").strip().split(',') for i in content]  #content
		return (header, data)

	def export_to_json(self,filename, listHistObj):
		try:
			os.stat(self.path_wu_history + self.name+self.split+"monthly data")
		except Exception as e:
			print("[Warn] Cannot locate the folder. Create a new one instead.")
			os.mkdir(self.path_wu_history + self.name+self.split+"monthly data")
		listJson = []
		for obj in listHistObj:
			dictObj = obj.dictData
			dictObj["date"] = obj.date
			listJson.append(dictObj)
		with open(self.path_wu_history + self.name+self.split+"monthly data"+self.split+filename, 'w') as f:
			f.write(json.dumps(listJson))


if __name__ == "__main__":
	year = "2016"
	nameCity = sys.argv[1]
	print("%s data processing..."%sys.argv[1]) 
	a = wu_hist(nameCity)
	#b = a.access_history()
	#print(b[0])
	listFile = os.listdir(a.path_wu_history + nameCity)
	for i in range(1,13):
		listHistObj = []
		for filename in listFile:
			pattern = "%s-%s-"%(year,str(i))
			try:
				filename.index(pattern)
				header, data = a.access_file(filename)
				listHistObj.append(wu_history_file(filename,header,data))
			except Exception as e:
				pass
		a.export_to_json(("%s-%s.json"%(year, str(i))),listHistObj)	
