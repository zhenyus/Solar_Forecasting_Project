from keras.layers import AtrousConvolution2D, SpatialDropout2D, Convolution2D, Merge


def dilate_resnet(input_layer, nb_filters=32, dilate_size=(0, 0)):
    highway_x = input_layer
    x = AtrousConvolution2D(nb_filters, 3, 3, activation='relu', atrous_rate=dilate_size, padding='causal')(input_layer)
    x = SpatialDropout2D(0.05)(x)
    x = Convolution2D(filters=nb_filters, kernel_size=(1, 1), padding='same')(x)

    res_x = Merge(mode='sum')([highway_x, x])
    return res_x
