

from keras.models import Input, Model
from keras.layers import Conv2D, Reshape, LSTM, Dense


def lstm(input_shape, nb_filter=32, kernel_size=(3, 3)):
    # input_shape = [steps, features, channels] or [steps, features]
    input_layer = Input(shape=input_shape)
    x = input_layer
    if len(input_shape) == 3:
        print("Use defined convolution kernel")
        x = Conv2D(filters=nb_filter, kernel_size=kernel_size, padding='same', activation='relu')(x)
        x = Conv2D(filters=nb_filter, kernel_size=kernel_size, padding='same', activation='relu')(x)
        features = x._keras_shape[2]
        x = Reshape((input_shape[0], features * nb_filter))(x)
    x = LSTM(12)(x)
    x = Dense(1, activation='relu')(x)

    model = Model(inputs=input_layer, outputs=[x])
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae'])
    return model
