import numpy
import math

from sklearn.preprocessing import MinMaxScaler
from sklearn.externals import joblib
from sklearn.metrics import mean_squared_error

from utils import *


# Hyperparameters
numpy.random.seed(1234)  # For reproduction
look_back = 120  # 5 days lookback
offset = 0  # offset days ahead prediction

dataset, target = load_data(city='Eugene', year=['2013', '2014', '2015'])
test_dataset, test_target = load_data(city='Eugene', year='2016')

# Put previous target data as features
dataset = numpy.hstack((dataset, target))
test_dataset = numpy.hstack((test_dataset, test_target))

# normalize the dataset
scaler = MinMaxScaler(feature_range=(0,1))
dataset = scaler.fit_transform(dataset)

scaler2 = MinMaxScaler(feature_range=(0,1))
target = scaler2.fit_transform(target)

test_dataset = scaler.transform(test_dataset)
test_target = scaler2.transform(test_target)

# 1
trainX, trainY = create_dataset(dataset, target, look_back, offset, sequence=True)
testX, testY = create_dataset(test_dataset,test_target,look_back, offset, sequence=True)

# Fit regression model
clf = joblib.load('svr_poly.pkl')

# Evaluation
train_predict = clf.predict(trainX).reshape((-1, 1))
test_predict = clf.predict(testX).reshape((-1, 1))

print(train_predict.shape, test_predict.shape)
# relu
train_predict = numpy.asarray([i if i>0 else 0.0 for i in train_predict]).reshape((-1,1))
test_predict = numpy.asarray([i if i>0 else 0.0 for i in test_predict]).reshape((-1,1))

# calculate root mean squared error
trainScore = math.sqrt(mean_squared_error(trainY[:, 0], train_predict[:, 0]))
print('Train Score: %.5f RMSE' % (trainScore))
testScore = math.sqrt(mean_squared_error(testY[:, 0], test_predict[:, 0]))
print('Test Score: %.5f RMSE' % (testScore))

# invert predictions
train_predict = scaler2.inverse_transform(train_predict)
trainY = scaler2.inverse_transform(trainY)
test_predict = scaler2.inverse_transform(test_predict)
testY = scaler2.inverse_transform(testY)

target = scaler2.inverse_transform(target)
test_target = scaler2.inverse_transform(test_target)

# train predictions for plotting
plot(train_predict, target, look_back, offset, title='Training Dataset Prediction')
# test predictions for plotting
plot(test_predict, test_target, look_back, offset, title='Test Dataset Prediction')

