from keras.layers import BatchNormalization, Activation, Add, Conv2D


def resnet_module(input_layer, num_kernel=32, batch_norm=False):
    '''
    Resnet block
    :param input_layer: previous layer from the model
    :param num_kernel: number of feature maps
    :param batch_norm: use batch normalization
    :return: model with resnet_block
    '''

    pathway1 = input_layer

    if batch_norm:
        pathway1 = BatchNormalization(axis=-1)(pathway1)
        pathway1 = Activation('relu')(pathway1)
    pathway1 = Conv2D(num_kernel, (3, 3), padding='same', data_format='channels_last')(pathway1)
    if batch_norm:
        pathway1 = BatchNormalization(axis=-1)(pathway1)
        pathway1 = Activation('relu')(pathway1)
    pathway1 = Conv2D(num_kernel, (3, 3), padding='same', data_format='channels_last')(pathway1)
    return Add()([pathway1, input_layer])
