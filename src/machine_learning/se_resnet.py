from keras.layers import BatchNormalization, Activation, Add, Conv2D, GlobalAveragePooling2D, Reshape, Dense, Multiply

import keras.backend as K
from keras import optimizers
from keras.layers import AtrousConvolution1D, SpatialDropout1D, Activation, Lambda, \
    Convolution1D, Merge, Dense, Flatten, Conv1D, Add
from keras.models import Input, Model



def se_resnet_module(input, kernel_size):
    '''
    Squeeze Excitation Resnet block
    :param input: previous layer from the model
    :return: model with resnet_block
    '''

    # Residual block
    pathway1 = BatchNormalization(axis=1)(input)
    pathway1 = Activation('relu')(pathway1)
    pathway1 = Conv2D(kernel_size, (3, 3), padding='same', data_format='channels_first')(pathway1)
    pathway1 = BatchNormalization(axis=1)(pathway1)
    pathway1 = Activation('relu')(pathway1)
    pathway1 = Conv2D(kernel_size, (3, 3), padding='same', data_format='channels_first')(pathway1)
    channels = pathway1._keras_shape[1]
    pathway2 = GlobalAveragePooling2D(data_format='channels_first')(pathway1)
    pathway2 = Reshape((1, 1, channels))(pathway2)
    pathway2 = Dense(channels // 2, activation='relu', kernel_initializer='he_normal', use_bias=False)(pathway2)
    pathway2 = Dense(channels, activation='sigmoid', kernel_initializer='he_normal', use_bias=False)(pathway2)
    pathway2 = Reshape((channels, 1, 1))(pathway2)
    highway = Multiply()([pathway2, pathway1])

    return Add()([highway, input])


