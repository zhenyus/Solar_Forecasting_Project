import math

import numpy
from keras.layers import Activation
from keras.layers import Conv2D
from keras.layers import Dense
from keras.layers import Input
from keras.layers import LSTM
from keras.layers import Reshape
from keras.models import Model
from resnet import resnet_module
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from utils import *

# Hyperparameters
numpy.random.seed(1234)  # For reproduction
look_back = 120  # 5 days lookback
offset = 0  # offset days ahead prediction
USE_BN = False
model_name = 'model_resnet.keras'

dataset, target = load_data(city='Eugene', year=['2013', '2014', '2015'])
test_dataset, test_target = load_data(city='Eugene', year='2016')

# Put previous target data as features
dataset = numpy.hstack((dataset, target))
test_dataset = numpy.hstack((test_dataset, test_target))
# normalizae the dataset
scaler = MinMaxScaler(feature_range=(0, 1))
dataset = scaler.fit_transform(dataset)

scaler2 = MinMaxScaler(feature_range=(0, 1))
target = scaler2.fit_transform(target)

test_dataset = scaler.transform(test_dataset)
test_target = scaler2.transform(test_target)

# 1
trainX, trainY = create_dataset(dataset, target, look_back, offset)
testX, testY = create_dataset(test_dataset, test_target, look_back, offset)

# reshape input to be [samples, channels, time steps, features]
feature_size = trainX.shape[2]
time_step = trainX.shape[1]
samples = trainX.shape[0]
trainX = trainX.reshape((-1, time_step, feature_size, 1))
testX = testX.reshape((-1, time_step, feature_size, 1))
input_shape = (time_step, feature_size, 1)

# create and fit the resnet network
input_layer = Input(shape=input_shape)
kernel_num = 32
x = Conv2D(kernel_num, (3, 3), input_shape=input_shape, padding='same', data_format='channels_last')(input_layer)
for i in range(3):
    x = resnet_module(x, kernel_num, batch_norm=USE_BN)
print(x._keras_shape)
x = Reshape((look_back, -1))(x)
print(x._keras_shape)
x = LSTM(12)(x)
x = Dense(1)(x)
x = Activation('relu')(x)

model = Model(inputs=input_layer, outputs=[x])
model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae'])
model.fit(trainX, trainY, epochs=50, batch_size=200, verbose=1)

# Save the model
model.save(model_name)

# make predictions
trainPredict = model.predict(trainX)
testPredict = model.predict(testX)

# Reshape
trainY = trainY.reshape(-1)
testY = testY.reshape(-1)

# calculate root mean squared error
trainScore = math.sqrt(mean_squared_error(trainY, trainPredict[:, 0]))
print('Train Score: %.5f RMSE' % (trainScore))
testScore = math.sqrt(mean_squared_error(testY, testPredict[:, 0]))
print('Test Score: %.5f RMSE' % (testScore))

# invert predictions
trainPredict = scaler2.inverse_transform(trainPredict)
trainY = scaler2.inverse_transform(trainY)
testPredict = scaler2.inverse_transform(testPredict)
testY = scaler2.inverse_transform(testY)

# train predictions for plotting
plot(train_predict, target, look_back, offset, title='Training Dataset Prediction')
# test predictions for plotting
plot(test_predict, test_target, look_back, offset, title='Test Dataset Prediction')
