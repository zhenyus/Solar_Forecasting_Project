import numpy
import pandas
import math

from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.externals import joblib
from pyflux import ARIMAX, Normal

from utils import *


# Hyperparameters
numpy.random.seed(1234)  # For reproduction
look_back = 1  # 5 days lookback
offset = 0  # offset days ahead prediction
year_list = ['2013', '2014', '2015', '2016']

for year in year_list:
	print('Set %s as validation set.'% (year))
	model_name = 'model_arimax_cv_%s.keras' % year
	dataset, target = load_data(city='Eugene', year=[i for i in year_list if i!=year])
	test_dataset, test_target = load_data(city='Eugene', year=year)

	# Put previous target data as features
	dataset = numpy.hstack((dataset, target))
	test_dataset = numpy.hstack((test_dataset, test_target))

	# normalize the dataset
	scaler = MinMaxScaler(feature_range=(0, 1))
	dataset = scaler.fit_transform(dataset)

	scaler2 = MinMaxScaler(feature_range=(0, 1))
	target = scaler2.fit_transform(target)

	test_dataset = scaler.transform(test_dataset)
	test_target = scaler2.transform(test_target)

	# 1
	trainX, trainY = create_dataset(dataset, target, look_back, offset, sequence=True)
	testX, testY = create_dataset(test_dataset, test_target, look_back, offset, sequence=True)


	data = numpy.hstack((trainX, trainY))
	df = pandas.DataFrame(data=data, columns=['sin_day', 'cos_day', 'sin_time', 'cos_time', 'Dewpoint', 'Windspeed',
	                                          'Temperature', 'Conditions', 'SI', 'target'])
	# Load the model
	model = joblib.load(model_name)

	# Evaluation
	# train

	data = numpy.hstack((trainX, trainY))
	df = pandas.DataFrame(data=data, columns=['sin_day', 'cos_day', 'sin_time', 'cos_time', 'Dewpoint', 'Windspeed',
	                                          'Temperature', 'Conditions', 'SI', 'target'])

	train_predict = model.predict(df.shape[0], df)
	# calculate root mean squared error
	a = trainY
	b = train_predict['target'].values
	print(a.shape, b.shape)
	trainScore = math.sqrt(mean_squared_error(a, b))
	print('train Score: %.5f RMSE' % (trainScore))


	# test
	data = numpy.hstack((testX, testY))
	df = pandas.DataFrame(data=data, columns=['sin_day', 'cos_day', 'sin_time', 'cos_time', 'Dewpoint', 'Windspeed',
	                                          'Temperature', 'Conditions', 'SI', 'target'])

	test_predict = model.predict(df.shape[0], df)
	# calculate root mean squared error
	a = testY
	b = test_predict['target'].values
	print(a.shape, b.shape)
	testScore = math.sqrt(mean_squared_error(a, b))
	print('Test Score: %.5f RMSE' % (testScore))
