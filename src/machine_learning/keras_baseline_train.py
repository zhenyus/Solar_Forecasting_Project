import math

import numpy
from lstm import lstm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from utils import *

# This is the baseline for solar radiation prediction project:
# Input: [8640, 1, 120, 9] ([samples, channels, steps, features])
# Model: 12LSTM - 1Dense - Relu
# Normalization: MinMaxScaler
# Optimization: adam

# Hyperparameters
numpy.random.seed(1234)  # For reproduction
look_back = 120  # 5 days lookback
offset = 0  # offset days ahead prediction
model_name = 'model_baseline.keras'

dataset, target = load_data(city='Eugene', year=['2013', '2014', '2015'])
test_dataset, test_target = load_data(city='Eugene', year='2016')
print(dataset.shape)
# Put previous target data as features
dataset = numpy.hstack((dataset, target))
test_dataset = numpy.hstack((test_dataset, test_target))
# normalizae the dataset
scaler = MinMaxScaler(feature_range=(0, 1))
dataset = scaler.fit_transform(dataset)

scaler2 = MinMaxScaler(feature_range=(0, 1))
target = scaler2.fit_transform(target)

test_dataset = scaler.transform(test_dataset)
test_target = scaler2.transform(test_target)

# 1
trainX, trainY = create_dataset(dataset, target, look_back, offset)
testX, testY = create_dataset(test_dataset, test_target, look_back, offset)

# reshape input to be [samples, channels, time steps, features]
feature_size = trainX.shape[2]
time_step = trainX.shape[1]
samples = trainX.shape[0]
input_shape = (time_step, feature_size)

baseline = True

# Set input shape, (time_step, feature_size) for basline, (time_step, feature_size, 1) for conv
if not baseline:
    trainX = trainX.reshape((-1, time_step, feature_size, 1))
    testX = testX.reshape((-1, time_step, feature_size, 1))
    input_shape = (time_step, feature_size, 1)
    model_name = 'model_32Conv_lstm.keras'
# create and fit the LSTM network

model = lstm(input_shape=input_shape)
model.fit(trainX, trainY, epochs=50, batch_size=50, verbose=1)

# Save the model
model.save(model_name)

# make predictions
train_predict = model.predict(trainX)
test_predict = model.predict(testX)

# Reshape
trainY = trainY.reshape(-1)
testY = testY.reshape(-1)

# calculate root mean squared error
trainScore = math.sqrt(mean_squared_error(trainY, train_predict[:, 0]))
print('Train Score: %.5f RMSE' % (trainScore))
testScore = math.sqrt(mean_squared_error(testY, test_predict[:, 0]))
print('Test Score: %.5f RMSE' % (testScore))

# invert predictions
train_predict = scaler2.inverse_transform(train_predict)
trainY = scaler2.inverse_transform(trainY)
test_predict = scaler2.inverse_transform(test_predict)
testY = scaler2.inverse_transform(testY)
target = scaler2.inverse_transform(target)
test_target = scaler2.inverse_transform(test_target)

# train predictions for plotting
plot(train_predict, target, look_back, offset, title='Training Dataset Prediction')
# test predictions for plotting
plot(test_predict, test_target, look_back, offset, title='Test Dataset Prediction')
