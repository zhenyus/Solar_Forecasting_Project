import math

import numpy
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from tcn import dilated_tcn
from utils import *

# This is the baseline for solar radiation prediction project:
# Input: [8640, 1, 120, 9] ([samples, channels, steps, features])
# Model: 12LSTM - 1Dense - Relu
# Normalization: MinMaxScaler
# Optimization: adam

# Set the index of channels, 'th' = theano with index 1, 'tf' = tensorflow with index 3
DIM_ORDERING = 'th'
if (DIM_ORDERING == 'th'):
    CONCAT_AXIS = 1
    data_format = "channels_first"
elif (DIM_ORDERING == 'tf'):
    CONCAT_AXIS = 3
    data_format = "channels_last"

# Hyperparameters
numpy.random.seed(1234)  # For reproduction
look_back = 1  # 5 days lookback
offset = 0  # offset days ahead prediction

year_list = ['2013', '2014', '2015', '2016']

for year in year_list:
    print('Set %s as validation set.' % (year))
    model_name = 'model_tcn_cv_%s.keras' % year
    dataset, target = load_data(city='Eugene', year=[i for i in year_list if i != year])
    test_dataset, test_target = load_data(city='Eugene', year=year)
    # Put previous target data as features
    dataset = numpy.hstack((dataset, target))
    test_dataset = numpy.hstack((test_dataset, test_target))
    # normalizae the dataset
    scaler = MinMaxScaler(feature_range=(0, 1))
    dataset = scaler.fit_transform(dataset)

    scaler2 = MinMaxScaler(feature_range=(0, 1))
    target = scaler2.fit_transform(target)

    test_dataset = scaler.transform(test_dataset)
    test_target = scaler2.transform(test_target)

    # 1
    trainX, trainY = create_dataset(dataset, target, look_back, offset, sequence=True)
    testX, testY = create_dataset(test_dataset, test_target, look_back, offset, sequence=True)

    # reshape input to be [samples, features, channels]
    feature_size = trainX.shape[1]
    samples = trainX.shape[0]
    input_shape = (1, feature_size)

    trainX = trainX.reshape((samples, feature_size, 1))
    testX = testX.reshape((testX.shape[0], feature_size, 1))

    model = dilated_tcn(num_feat=1, num_classes=1, nb_filters=16,
                        kernel_size=8, dilatations=[1, 2, 4, 8], nb_stacks=2, max_len=feature_size,
                        activation='relu', use_skip_connections=False,
                        return_param_str=False, output_slice_index=None,
                        regression=True)
    model.fit(trainX, trainY, epochs=100, batch_size=50, verbose=1)

    # Save the model
    model.save(model_name)

    # make predictions
    train_predict = model.predict(trainX)
    test_predict = model.predict(testX)

    # Reshape
    trainY = [trainY.reshape(-1)]
    testY = [testY.reshape(-1)]

    # calculate root mean squared error
    trainScore = math.sqrt(mean_squared_error(trainY[0], train_predict[:, 0]))
    print('Train Score: %.5f RMSE' % (trainScore))
    testScore = math.sqrt(mean_squared_error(testY[0], test_predict[:, 0]))
    print('Test Score: %.5f RMSE' % (testScore))
