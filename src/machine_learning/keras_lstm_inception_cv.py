import math

import matplotlib.pyplot as plt
import numpy
from keras.layers import Dense
from keras.layers import Input
from keras.layers import LSTM
from keras.layers import Reshape
from keras.models import Model
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from utils import *
from inception import *

# global constants
WEIGHT_DECAY = 0.0005  # L2 regularization factor
USE_BN = False  # whether to use batch normalization
CONCAT_AXIS = -1
data_format = "channels_last"

numpy.random.seed(1234)

look_back = 120  # 5 days lookback
offset = 0  # offset days ahead prediction

year_list = ['2013', '2014', '2015', '2016']

for year in year_list:
    print('Set %s as validation set.'% (year))
    model_name = 'model_inception_cv_%s.keras' % year
    dataset, target = load_data(city='Eugene', year=[i for i in year_list if i!=year])
    test_dataset, test_target = load_data(city='Eugene', year=year)

    # Put previous target data as features
    dataset = numpy.hstack((dataset, target))
    test_dataset = numpy.hstack((test_dataset, test_target))
    # normalizae the dataset
    scaler = MinMaxScaler(feature_range=(0, 1))
    dataset = scaler.fit_transform(dataset)

    scaler2 = MinMaxScaler(feature_range=(0, 1))
    target = scaler2.fit_transform(target)

    test_dataset = scaler.transform(test_dataset)
    test_target = scaler2.transform(test_target)

    # 1
    trainX, trainY = create_dataset(dataset, target, look_back, offset)
    testX, testY = create_dataset(test_dataset, test_target, look_back, offset)

    # reshape input to be [samples, channels, time steps, features]
    feature_size = trainX.shape[2]
    time_step = trainX.shape[1]
    samples = trainX.shape[0]
    input_shape = (time_step, feature_size, 1)

    trainX = trainX.reshape((-1, time_step, feature_size, 1))
    testX = testX.reshape((-1, time_step, feature_size, 1))

    # create and fit the LSTM network
    input_layer = Input(shape=input_shape)
    x = conv_layer(input_layer, nb_filter=32, nb_row=3, nb_col=3, data_format=data_format)
    x = inception_module(x, params=[(64,), (96, 128), (16, 32), (32,)],
                         data_format=data_format, concat_axis=CONCAT_AXIS)
    x = Reshape((look_back, -1))(x)
    x = LSTM(12)(x)
    x = Dense(1, activation='relu')(x)

    model = Model(inputs=input_layer, outputs=[x])
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae'])
    model.fit(trainX, trainY, epochs=50, batch_size=50, verbose=1)

    # Save the model
    model.save(model_name)

    # make predictions
    train_predict = model.predict(trainX)
    test_predict = model.predict(testX)
    # Reshape
    trainY = trainY.reshape(-1)
    testY = testY.reshape(-1)

    # calculate root mean squared error
    trainScore = math.sqrt(mean_squared_error(trainY, train_predict[:, 0]))
    print('Train Score: %.5f RMSE' % (trainScore))
    testScore = math.sqrt(mean_squared_error(testY, test_predict[:, 0]))
    print('Test Score: %.5f RMSE' % (testScore))

