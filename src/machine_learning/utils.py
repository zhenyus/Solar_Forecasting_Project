import platform
import sys

import matplotlib
import matplotlib.pyplot as plt
import numpy
import pandas

font = {'family' : 'normal',
        'size'   : 24}

matplotlib.rc('font', **font)
matplotlib.rc('xtick', labelsize=24)
matplotlib.rc('ytick', labelsize=24)

def load_data(city='Eugene', year='2016',index=0):
    if platform.system() == 'Windows':
        split = "\\"
    else:
        split = "/"

    if isinstance(year, str):
        year = [year]

    # Load Data
    source_directory = "..%s..%sdata%sformat_data%s%s%s" % (split, split, split, split, city, split)
    dataset = None
    target = None

    for y in year:
        dataframe = pandas.read_csv(source_directory + '%s_dataset.txt' % y, sep='\t', engine='python')
        if (dataframe.isnull().any().any()):
            print("Dataset has nan value! Abort.")
            sys.exit()
        dataset_temp = dataframe.values
        dataset_temp.astype('float32')
        dataframe = pandas.read_csv(source_directory + '%s_target.txt' % y, sep='\t', engine='python')
        if (dataframe.isnull().any().any()):
            print("target file has nan value! Abort.")
            sys.exit()
        target_temp = dataframe.values
        # access the target column
        target_temp = target_temp[:, index].reshape(-1, 1)
        target_temp.astype('float32')
        if dataset is None:
            dataset = dataset_temp
            target = target_temp
        else:
            dataset = numpy.vstack((dataset, dataset_temp))
            target = numpy.vstack((target, target_temp))
    return dataset, target


# convert an array of values into a dataset matrix
def create_dataset(dataset, target, look_back=1, offset=0, sequence=False):
    dataX, dataY = [], []
    for i in range(dataset.shape[0] - look_back - offset):
        a = dataset[i:(i + look_back), :]
        dataX.append(a)
        dataY.append(target[i + look_back + offset])
    dataX, dataY = numpy.array(dataX), numpy.array(dataY)
    if sequence is True:
        dataX = dataX.reshape((dataset.shape[0] - look_back - offset, -1))
    return dataX, dataY


def plot(predict, target, look_back, offset, title='', day_flag=1):
    # data plot, target and predict value should be transformed inversely before
    predict_plot = numpy.empty_like(target + look_back + offset)
    predict_plot[:, :] = numpy.nan
    predict_plot[look_back + offset:, :] = predict
    plt.figure()
    #plt.title(title)
    if day_flag == 1:
        target_x = numpy.asarray([i/24 for i in range(target.shape[0])])
        predict_x = numpy.asarray([i/24 for i in range(predict_plot.shape[0])])
        plt.plot(target_x, target, 'b', label='Real')
        plt.plot(predict_x, predict_plot, 'r', label='Predict')
        plt.xlabel('Day')
    else:
        target_x = numpy.asarray([i for i in range(target.shape[0])])
        predict_x = numpy.asarray([i for i in range(predict_plot.shape[0])])
        plt.plot(target_x, target, 'b', label='Real')
        plt.plot(predict_x, predict_plot, 'r', label='Predict')
        plt.xlabel('Hour')
    plt.ylabel('Solar Irradiance')
    plt.legend()
    plt.show()


def data_generator(dataset, target, look_back=1, offset=0, batch_size=1):
    for i in range(dataset.shape[0] - look_back - offset):
        dataX = numpy.array((batch_size, look_back, dataset.shape[1]))
        for j in range(batch_size):
            a = dataset[i + j:(i + j + look_back), :]
            dataX[j] = a
        dataY = target[i + look_back + offset: i + look_back + offset + batch_size]
        yield dataX, dataY
