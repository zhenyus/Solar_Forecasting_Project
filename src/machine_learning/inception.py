from keras import regularizers
from keras.layers import BatchNormalization
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import concatenate


def inception_module(x, params, data_format, concat_axis,
                     strides=(1, 1), activation='relu',
                     padding='same', weight_decay=None):
    (branch1, branch2, branch3, branch4) = params

    if weight_decay:
        kernel_regularizer = regularizers.l2(weight_decay)
        bias_regularizer = regularizers.l2(weight_decay)
    else:
        kernel_regularizer = None
        bias_regularizer = None

    pathway1 = Conv2D(branch1[0], (1, 1),
                      strides=strides,
                      activation=activation,
                      padding=padding,
                      kernel_regularizer=kernel_regularizer,
                      bias_regularizer=bias_regularizer,
                      use_bias=False,
                      data_format=data_format)(x)

    pathway2 = Conv2D(branch2[0], (1, 1),
                      strides=strides,
                      activation=activation,
                      padding=padding,
                      kernel_regularizer=kernel_regularizer,
                      bias_regularizer=bias_regularizer,
                      use_bias=False,
                      data_format=data_format)(x)
    pathway2 = Conv2D(branch2[1], (3, 3),
                      strides=strides,
                      activation=activation,
                      padding=padding,
                      kernel_regularizer=kernel_regularizer,
                      bias_regularizer=bias_regularizer,
                      use_bias=False,
                      data_format=data_format)(pathway2)

    pathway3 = Conv2D(branch3[0], (1, 1),
                      strides=strides,
                      activation=activation,
                      padding=padding,
                      kernel_regularizer=kernel_regularizer,
                      bias_regularizer=bias_regularizer,
                      use_bias=False,
                      data_format=data_format)(x)
    pathway3 = Conv2D(branch3[1], (5, 5),
                      strides=strides,
                      activation=activation,
                      padding=padding,
                      kernel_regularizer=kernel_regularizer,
                      bias_regularizer=bias_regularizer,
                      use_bias=False,
                      data_format=data_format)(pathway3)

    pathway4 = MaxPooling2D(pool_size=(1, 1), data_format=data_format)(x)
    pathway4 = Conv2D(branch4[0], (1, 1),
                      strides=strides,
                      activation=activation,
                      padding=padding,
                      kernel_regularizer=kernel_regularizer,
                      bias_regularizer=bias_regularizer,
                      use_bias=False,
                      data_format=data_format)(pathway4)
    return concatenate([pathway1, pathway2, pathway3, pathway4], axis=concat_axis)


def conv_layer(x, nb_filter, nb_row, nb_col, data_format,
               strides=(1, 1), activation='relu',
               padding='same', weight_decay=None, batch_norm=False):
    if weight_decay:
        kernel_regularizer = regularizers.l2(weight_decay)
        bias_regularizer = regularizers.l2(weight_decay)
    else:
        kernel_regularizer = None
        bias_regularizer = None

    x = Conv2D(nb_filter, (nb_row, nb_col),
               strides=strides,
               activation=activation,
               padding=padding,
               kernel_regularizer=kernel_regularizer,
               bias_regularizer=bias_regularizer,
               use_bias=False,
               data_format=data_format)(x)
    if batch_norm and data_format == 'channels_last':
        x = BatchNormalization()(x)
    elif batch_norm and data_format == 'channels_first':
        x = BatchNormalization(axis=1)(x)
    return x
