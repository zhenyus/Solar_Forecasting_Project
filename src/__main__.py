from crawler import *

# Initialize Crawlers
Eugene = crawler("Eugene", "OR")
Bend = crawler("Bend", "OR")
Seattle = crawler("Seattle", "WA")
# Corvallis = crawler("Corvallis", "OR")

Eugene.write_data()
Bend.write_data()
Seattle.write_data()
# Corvallis.write_data()
# print time.ctime()