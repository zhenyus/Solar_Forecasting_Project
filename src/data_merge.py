from __future__ import print_function
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
import sys

from uo_util import *
from wu_util import *
from suntime import *

class data_merge(uo_util, wu_util, suntime):
	def __init__(self, name):
		wu_util.__init__(self, name)
		uo_util.__init__(self)
		suntime.__init__(self, name, 2016)
		self.path_output = "..\\data\\output\\"
		self.name = name
		self.name_file = {"Eugene":"EUPO1611.txt", "Bend":"BERQ1611.txt", "Seattle": "SEPQ1611.txt"}
		self.name_code = {"Eugene": ["1000", "2010", "3005"], "Bend": ["1001", "2011", "3001"],
						  "Seattle": ["1000", "2010", "3001"]}
		self.uo_data = self.fast_access(self.name, self.name_file[self.name], self.name_code[self.name])
		# print self.uo_data_eugene
		self.wu_data = self.import_data_wu(name)
		#self.wu_data,self.filtered_data = self.filter_time()

	def merge(self):
		output = []
		for key, item in enumerate(self.uo_data):
			# print(item,  item[0]["day"], item[0]["time"][:2], item[0]["time"][-2:])
			result = self.access_data(None, "11", item[0]["day"], item[0]["time"][:2], item[0]["time"][-2:])
			if result:
				for i in result:
					for idx, value in enumerate(self.name_code[self.name]):
						i[value] = item[1][idx]
				# if item[1][0] != 0 or item[1][1] != 0 or item[1][2] != 0:
				output.append(result)
		# print len(output)
		return output

	def get_output(self, dataset):
		f = open(self.path_output + self.name, 'w')
		f.write("\t".join(a.list_key_name) + '\t' + "\t".join(a.name_code[a.name])+'\n')
		for data in dataset:
			for i in data:
				string = ""
				for key in a.list_key_name:
					string += str(i[key]) + '\t'
				for key in a.name_code[a.name]:
					string += str(i[key]) + '\t'
				f.write(string+'\n')
		f.close()
		# for key2, item2 in wu_data.iteritems():
		#	  # print item2["hour"] + item2["minute"], item2["day"], item2["month"]
		#	  print int(item[0]["day"]) - 274
		#	  if item[0]["time"] == item2["hour"] + item2["minute"] and int(item[0]["day"]) - 274 == int(
		#			  item2["day"]) and item2["month"] == "10":
		#		  print item, item2
	def export_to_csv(self, header=None, content=None, filename = "default.csv" ):
		with open(filename, 'w', newline='') as csvfile:
			spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|',quoting=csv.QUOTE_MINIMAL)
			spamwriter.writerow(header)
			for line in content:
				spamwriter.writerow(line)

	def filter_time(self): ##filter wu_data
		sunrise_hour = self.range[0]/60
		sunrise_min = self.range[0]%60
		sunset_hour = self.range[1]/60
		sunset_min = self.range[1]%60
		filter_dict = {} 
		filtered_dict = {}
		for key, value in self.wu_data.items():
			if (int(value['hour'])<=sunrise_hour and int(value['minute']) <=sunrise_min):
				filtered_dict[key]=value
			elif (int(value['hour'])>=sunset_hour and int(value['minute'])>=sunset_min):
				filtered_dict[key]=value
			else:
				filter_dict[key]=value
		return (filter_dict,filtered_dict)

if __name__ == '__main__':
	a = data_merge("Bend")
	print("The suntime is from %d to %d" %(a.range[0], a.range[1]))
#	print(a.filtered_data)
	dataset = a.merge()
	content = []
	# a.get_output(dataset)
	header = a.list_key_name+a.name_code[a.name]
	# print(header)
	# print("\t".join(a.list_key_name) + '\t' + "\t".join(a.name_code[a.name]))
	for data in dataset:
		for i in data:
			# string = ""
			line = []
			for key in a.list_key_name:
				# string += str(i[key]) + '\t'
				line.append(str(i[key]))
			for key in a.name_code[a.name]:
				# string += str(i[key]) + '\t'
				line.append(str(i[key]))
			# print(string)
			content.append(line)
	a.export_to_csv(header, content)
	print("Export to csv file.\n")
