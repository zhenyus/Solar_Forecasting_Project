from __future__ import print_function
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import platform
import sys
import os
import re
import json

class condition_process(object):
	def __init__(self, directory):
		self.directory = directory


	def parse_json(self):
		jsonfile = []
		for file in os.listdir(self.directory):
			with open(self.directory+'/'+file) as f:
				content = f.readline()
			jsonfile+=json.loads(content)

		return jsonfile

if __name__ == '__main__':
	directory = '/home/campus19/zhenyus/Documents/Research/Solar_Forecasting_Project/data/wu_history/Bend/processed'
	a= condition_process(directory)
	listJson = a.parse_json()
	listCondition = []
	for json in listJson:
		listCondition += json['Conditions']
	print(set(listCondition))
