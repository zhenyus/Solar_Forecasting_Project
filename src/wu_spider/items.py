# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

class WuItem(Item):
	# define the fields for your item here like:
	# name = scrapy.Field()
	# title = scrapy.Field()
	content = Field()
	Time = Field()
	Temperature = Field()
	Windchill = Field()
	DewPoint = Field()
	Humidity = Field()
	Pressure = Field()
	Visibility = Field()
	WindDirection = Field()
	WindSpeed = Field()
	GustSpeed = Field()
	Precip = Field()
	Events = Field()
	Conditions = Field()
