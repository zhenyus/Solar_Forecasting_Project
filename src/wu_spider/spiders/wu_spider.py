from scrapy.spider import Spider  
from scrapy.selector import Selector 

from wu_spider.items import WuItem

class WUSpider(Spider):  
	name = "wu"  
	allowed_domains = [" www.wunderground.com"]  
	start_urls = [	
	"https://www.wunderground.com/history/airport/KBDN/2016/12/30/DailyHistory.html?req_city=Bend&req_state=OR"
]  

	def parse(self, response):
		sel = Selector(response)
		header_row = sel.xpath('//*[@id="obsTable"]/thead/tr/th')
		items = []
		item = WuItem()
		item_buffer = []
		for element in header_row:
			item_buffer.append(element.xpath('text()').extract())

		item['content'] = item_buffer
		item_buffer = []
		items.append(item)

		content_row = sel.xpath('//*[@id="obsTable"]/tbody/tr')
		for row in content_row:
			item = WuItem()
			item['content'] = row.xpath('td/text()').extract()
			items.append(item)
		return items
