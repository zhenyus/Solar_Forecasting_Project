from scrapy.spider import Spider  
from scrapy.selector import Selector 

from wu_spider.items import WuItem

class BendSpider(Spider):  
	name = "Bend"  
	allowed_domains = ["www.wunderground.com"]
	year = 2013
	day_list = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
	start_urls = []
	url = "https://www.wunderground.com/history/airport/KBDN/%s/%s/%s/DailyHistory.html?req_city=Bend&req_state=OR"
	for month in range(1, 13):
		for day in range(1, day_list[month]+1):
			start_urls.append(url % (str(year), str(month), str(day)))
#####TEST#####
#	start_urls.append(url % (2015, 1,1))

	def access_value(self, row, index):
		path = "td[%s]/span/span[1]/text()"%index
		path2 = "td[%s]/text()"%index
		try:
			value = [a.strip() for a in row.xpath(path).extract()]
			if value == []:
				return [a.strip() for a in row.xpath(path2).extract()]
			else:
				return value
		except:
			return row.xpath(path2).extract()

	def parse(self, response):
		sel = Selector(response)
#		header_row = sel.xpath('//*[@id="obsTable"]/thead/tr')
		items = []
#		item = WuItem()
#		item_buffer = []
#		for element in header_row:
#			item_buffer.append(element.xpath('th/text()').extract())
#			item = WuItem()
#			item['content'] = element.xpath('th/text()').extract()
#			items.append(item)
#		item['content'] = item_buffer
#		item_buffer = []
#		items.append(item)

		item = WuItem()
		content_row = sel.xpath('//*[@id="obsTable"]/tbody/tr')
		item['Time'] = []
		item['Temperature'] = []
		item['Windchill'] = []
		item['DewPoint'] = []
		item['Humidity'] = []
		item['Pressure'] = []
		item['Visibility'] = []
		item['WindDirection'] = []
		item['WindSpeed'] = []
		item['GustSpeed'] = []
		item['Precip'] = []
		item['Events'] = []
		item['Conditions'] = []

		for row in content_row:
#			item['content'] = row.xpath('td/text()').extract()
			item['Time'] += self.access_value(row, 1) 
			item['Temperature'] += self.access_value(row, 2)
			item['Windchill'] += self.access_value(row, 3)
			item['DewPoint'] += self.access_value(row, 4)
			item['Humidity'] += self.access_value(row, 5)
			item['Pressure'] += self.access_value(row, 6)
			item['Visibility'] += self.access_value(row, 7)
			item['WindDirection'] +=  self.access_value(row, 8)
			item['WindSpeed'] += self.access_value(row, 9)
			item['GustSpeed'] += self.access_value(row, 10)
			item['Precip'] += self.access_value(row, 11)
			item['Events'] += self.access_value(row, 12)
			item['Conditions'] += self.access_value(row, 13)
			
		items.append(item)
		return items
