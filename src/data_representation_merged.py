from __future__ import print_function
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import platform
import sys
import os
import re
import math
##This file is for merged data representation, with time representation by cosine function with date and clock time repectively. 

class data_representation_merged(object):
	def __init__(self, city, year):
	# year not using right now
		if platform.system() == 'Windows':
			self.split = "\\"
		else:
			self.split = "/"

		self.city = city
		self.year = year
		self.sourceDirectory = 	"..%sdata%smerged_data%s%s%s" % (self.split, self.split, self.split, self.city, self.split)
		self.targetDirectory = 	"..%sdata%s%s%s" % (self.split, self.split, self.city, self.split)
	
	def import_data(self, filename):
		with open(self.sourceDirectory+filename, 'r') as f:
			content = f.readlines()
		header = content.pop(0).strip().split('\t')
		data = []
		for item in content:
			data.append(item.strip().split('\t'))
		return header, data
	
	def export_data(self, header, data, filename):
		if os.path.exists(self.targetDirectory) is False:
			os.mkdir(self.targetDirectory)
		with open(self.targetDirectory + filename, 'w') as f:
			f.write("\t".join(header)+'\n')
			for row in data:
				f.write("\t".join(row)+'\n')

	def circular_norm(self, value, period):
		return math.sin(int(value) * period), math.cos(int(value) * period)

if __name__ == '__main__':
	city = "Bend"
	year = "2016"
	a = data_representation_merged(city, year)
	listFilename =  os.listdir(a.sourceDirectory)
#	for filename in listFilename:
	for filename in listFilename:
		header, data = a.import_data(filename)
		outputHeader = ["tod_sin", "tod_cos", "t_sin", "t_cos"] + header[2:] 
		outputData = [] 
		for row in data:
			tod_sin, tod_cos = a.circular_norm(int(row[0]), 365)
			t_sin, t_cos = a.circular_norm(int(row[1])/100*60, 16)
			listData = [str(tod_sin), str(tod_cos), str(t_sin), str(t_cos)] + row[2:]
			outputData.append(listData)
		a.export_data(outputHeader, outputData, filename)
	print("Done")
