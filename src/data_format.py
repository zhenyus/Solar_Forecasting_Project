from __future__ import print_function

import os
import platform
import sys

import numpy as np


# This file is to serialize the data into tensorflow format

class data_format(object):
    def __init__(self, city, year="2016"):
        if platform.system() == 'Windows':
            self.split = "\\"
        else:
            self.split = "/"

        self.city = city
        self.year = year
        self.sourceDirectory = "..%sdata%scircular_norm_data%s%s%s" % (
        self.split, self.split, self.split, self.city, self.split)
        self.targetDirectory = "..%sdata%sformat_data%s%s%s" % (
        self.split, self.split, self.split, self.city, self.split)

    def import_txt(self, filename):
        with open(self.sourceDirectory + filename, 'r') as f:
            content = f.readlines()
        header = content.pop(0).strip().split('\t')
        data = []
        for row in content:
            data.append(row.strip().split('\t'))
        return header, data


if __name__ == '__main__':
    a = data_format(sys.argv[1], sys.argv[2])
    dataset = []
    for month in range(1, 13):
        filename = "%s%02d.txt" % (a.year, month)
        header, data = a.import_txt(filename)
        dataset += data
    #	dataset = np.asarray(dataset).astype('float32')
    dataset = np.asarray(dataset)
    # Extra process
    for i in range(dataset.shape[0]):
        for j in range(dataset.shape[1] - 1):  # Ignore Condition column, hardcoded
            if (dataset[i, j] == '-' or dataset[i, j] == 'N/A%'):
                dataset[i, j] = dataset[i - 1, j]

            #   import condition dictionary
    with open('condition_dict.txt', 'r') as f:
        condition_dict = eval(f.readline())

    #	print(np.isnan(dataset[:,9]).any().any())
    train = dataset[:, [0, 1, 2, 3, 7, 8, 9, 10]]
    for i, v in enumerate(train):
        train[i, -1] = condition_dict[train[i, -1].strip()]

    target = dataset[:, [4, 5, 6]]
    if os.path.exists(a.targetDirectory) is False:
        os.mkdir(a.targetDirectory)
    filename = "%s_dataset.txt" % a.year
    dataset_header = "sin_day\tcos_day\tsin_time\tcos_time\tDewPoint\tWindSpeed\tTemperature\tConditions"
    np.savetxt(a.targetDirectory + filename, train, delimiter='\t', header=dataset_header, comments='', fmt='%10s')
    filename = "%s_target.txt" % a.year
    np.savetxt(a.targetDirectory + filename, target, delimiter='\t', header='1\t2\t3', comments='', fmt='%s')
