from __future__ import print_function
# !/usr/bin/env python
# -*- coding: utf-8 -*-

import platform
import sys
import os
import re
import json
import operator


##This file is for merging preprocessed monthly data of uo and wu

class data_merge(object):
    def __init__(self, city, year="2016"):
        if platform.system() == 'Windows':
            self.split = "\\"
        else:
            self.split = "/"

        self.city = city
        if (city == "Eugene"):
            self.citycode = "EUPO"
        elif (city == "Bend"):
            self.citycode = "BERQ"
        elif (city == "Seattle"):
            self.citycode = "SEPQ"
        self.year = year
        self.wuDirectory = "..%sdata%swu_history%s%s%sprocessed%s" % (
        self.split, self.split, self.split, self.city, self.split, self.split)
        self.uoDirectory = "..%sdata%suoregon%s%s%ssuntime_filter%s" % (
        self.split, self.split, self.split, self.city, self.split, self.split)
        self.targetDirectory = "..%sdata%smerged_data%s%s%s" % (
        self.split, self.split, self.split, self.city, self.split)

    def import_json(self, filename):
        with open(self.wuDirectory + filename, 'r') as f:
            content = json.loads(f.readline().strip())
        # print(content)
        return content

    def import_txt(self, filename):
        with open(self.uoDirectory + filename, 'r') as f:
            content = f.readlines()
        header = content.pop(0).strip().split('\t')
        data = []
        for row in content:
            data.append(row.strip().split('\t'))
        return header, data

    def export_txt(self, filename, data):
        if os.path.exists(self.targetDirectory) is False:
            os.mkdir(self.targetDirectory)
        with open(self.targetDirectory + filename, 'w') as f:
            header = ['day', 'time', '1', '2', '3', 'DewPoint', 'WindSpeed', 'Temperature', 'Conditions']
            f.write("\t".join(header) + '\n')
            for day in data:
                for row in day:
                    # print(row)
                    f.write("\t".join(row) + '\n')

    def get_wu_file(self, filename):
        # This method is for new wu file in processed folder. One file contains whole year data.
        pass

    def get_timespan(self, data):
        # We need to get the correct time span (This takes O(n) for selecting the most frequent time span in array)
        timespan = {}
        for index, time in enumerate(data):
            try:
                key = data[index + 1] - time
                if key in timespan:
                    timespan[key] += 1
                else:
                    timespan[key] = 1
            except:
                pass
        return max(timespan, key=timespan.get)

    def get_offset(self, data, resolution):
        # We need to get the correct measure offset (Takes O(n))
        offset = {}
        for index, time in enumerate(data):
            try:
                key = time % resolution - resolution
                if key in offset:
                    offset[key] += 1
                else:
                    offset[key] = 1
            except:
                pass
        return max(offset, key=offset.get)


if __name__ == '__main__':
    ## City, year
    a = data_merge(sys.argv[1], sys.argv[2])

    for mon in range(1, 13):
        month = str(mon)
        jsonFilename = "%s-%s.json" % (a.year, month)  # Get wu file
        uoFilename = "%s%s%s.txt" % (a.citycode, a.year[-2:], month.zfill(2))  # Get uo data
        outputFilename = "%s%s.txt" % (a.year, month.zfill(2))
        print(jsonFilename, uoFilename, outputFilename)
        wuFile = a.import_json(jsonFilename)
        header, uoFile = a.import_txt(uoFilename)
        # start_time = int(uoFile[0][1])/100*60 + int(uoFile[0][1])%100
        start_time = 60
        end_time = 1440
        resolution = 60
        #		offset = 15
        #		measure_offset = wuFile[0]['Time'][0]%60 - resolution # This number is to avoid errors from  the measurement from wu
        #		time_span = wuFile[0]['Time'][1] - wuFile[0]['Time'][0]
        #		time_span = 60 #set 60 by default, this should be improved
        #		time_span_offset = int(60 / time_span)
        #		print(time_span_offset)
        #		print(wuFile[0]['Time'])
        #		print(start_time, measure_offset)
        #		start_index = wuFile[0]['Time'].index(start_time + measure_offset)
        #		end_index = wuFile[0]['Time'].index(end_time + measure_offset)
        #		print (start_index, end_index)
        outputData = []
        day = 0
        for day, data in enumerate(wuFile):
            time_span = a.get_timespan(data['Time'])
            time_span_offset = int(60 / time_span)
            #			measure_offset = data['Time'][0]%60 - resolution # This number is to avoid errors from  the measurement from wu
            measure_offset = a.get_offset(data['Time'], resolution)
            #			print(data['Time'])
            start_index = data['Time'].index(
                start_time + measure_offset)  # Because the daily data measurement varies differently.
            end_index = data['Time'].index(end_time + measure_offset)
            tempData = []
            index = 0
            tempTime = [i for i in data['Time'][start_index:end_index + 1:time_span_offset]]
            tempDew = [i for i in data['DewPoint'][start_index:end_index + 1:time_span_offset]]
            tempWind = [i for i in data['WindSpeed'][start_index:end_index + 1:time_span_offset]]
            tempTemp = [i for i in data['Temperature'][start_index:end_index + 1:time_span_offset]]
            tempCondition = [i for i in data['Conditions'][start_index:end_index + 1:time_span_offset]]
            tempUO = [i for i in uoFile if
                      (int(i[0]) == day + 1) and ((int(i[1]) / 100 * 60 + int(i[1]) % 100) % resolution == 0)]
            # Temperary fix
            tempUO = tempUO[1:]
            for element in tempUO:
                element = ["%.1f" % float(i) for i in element]
                tempData.append(element + [str(tempDew[index]), str(tempWind[index]), str(tempTemp[index]),
                                           str(tempCondition[index])])
                index += 1
            outputData.append(tempData)
        a.export_txt(outputFilename, outputData)
