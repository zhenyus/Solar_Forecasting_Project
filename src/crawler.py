from __future__ import print_function
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import time
from urllib.request import urlopen
import os


class crawler(object):
	def __init__(self, city="", states="", year = 2017):
		self.dict_city = {"Seattle": ['KBFI', 'Seattle', 'Washington', 'WA'],
											"Eugene": ['KEUG', 'Eugene', 'Oregon', 'OR'],
											"Bend": ['KBDN', 'Bend', 'Oregon', 'OR'],
											"Dallas": ['KDAL', 'Dellas', 'Texas', 'TX']}
		self.city = city
		self.states = states
		self.year = year
		#self.output = self.get_data()
		self.absDirectory = "/home/campus19/zhenyus/Documents/Research/Solar_Forecasting_Project"
		self.rltDirectory = ".."
		self.time = time.ctime().split(" ")

	def get_data(self):
		url = 'http://api.wunderground.com/api/e78ff52152871212/geolookup/conditions/q/%s/%s.json' % (
				self.states, self.city)
		print(url)
		f = urlopen(url)
		# f = open("data")
		json_string = f.read().decode("utf-8")	
		parsed_json = json.loads(json_string)
		# location = parsed_json['location']['city']
		# temp_f = parsed_json['current_observation']['temp_f']
		# print "Current temperature in %s is: %s" % (location, temp_f)
		f.close()
		return parsed_json

	def write_data(self, path="weather_underground", fname = "", content = ""):
		filepath = "%s/data/%s/%s" % (self.absDirectory, path, self.city)
		# filepath = "%s\\data\\%s\\%s" % (self.rltDirectory, path, self.city)
		try:
				os.stat(filepath)
		except Exception as e:
				print("[Warn] Cannot locate the folder. Create a new one instead.")
				os.mkdir(filepath)
		filename = filepath + "/%s_%s_%s_%s_%s_%s" % (
				self.states, self.city, self.time[1], self.time[2], self.time[4],
				self.time[3].replace(":", "_"))
		if content == "":
				fp = open(filename, 'w')
				fp.writelines(json.dumps(self.output, indent=4))
		else:
				fp = open(filepath + "/" + fname, 'w')
				fp.writelines(content)
		fp.close()

	def get_History(self):
		dict_month = {1:31, 2:29, 3:31, 4:30, 5:31, 6:30, 7:31, 8:31, 9:30, 10:31, 11:30, 12:30}
		airport_code, city_name, state_name, state_id = self.dict_city[self.city]
		print(airport_code, city_name, state_name, state_id)
		#year = 2017 #We use argument instead now
		month = 1
		day = 1
		for month in dict_month.keys():
				for day in range(1, dict_month[month]+1):
						url = "http://www.wunderground.com/history/airport/%s/%s/%s/%s/DailyHistory.html?req_city=%s&req_state=%s&req_statename=%s&format=1" % (
								airport_code, self.year, month, day, city_name, state_id, state_name)
						print(url)
						f = urlopen(url)
						content = f.read().decode('utf-8')
						f.close()
						self.write_data("wu_history", "%s-%s-%s" % (self.year, month, day), content)


if __name__ == '__main__':
	# a = crawler("Eugene", "OR", 2015)
	a = crawler("Bend", "OR", 2015)
	# a = crawler("Seattle", "WA")
	#a = crawler("Dallas", "TX")
	a.get_History()
