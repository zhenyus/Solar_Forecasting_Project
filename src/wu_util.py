#!/usr/bin/env python
# -*- coding: utf-8 -*-

## This class is for Weather Underground json file resolution ##
from __future__ import print_function
import os
import json
import sys
import re
import platform

class wu_util(object):
	def __init__(self, name):
		self.name = name
		if platform.system() == 'Windows':
			self.path_wu = "..\\data\\weather_underground\\"
		else:
			self.path_wu = "../data/weather_underground/"
		self.dict_calendar = {"Jan": 1, "Feb": 2, "Mar": 3, "Apr": 4, "May": 5, "Jun": 6, "July": 7, "Aug": 8, "Sep": 9, "Oct": 10, "Nov": 11, "Dec": 12}
		# Hardcoded station
		self.weather_cat = {"Clear": 1, "Fog": 2, "Light Rain": 3, "Mist": 4, "Mostly Cloudy": 5, "Overcast": 6, "Partly Cloudy": 7, "Rain": 8, "Scattered Clouds": 9, "Shallow Fog": 10, "Unknown": 11, "Light Drizzle": 12, "Haze": 13, "Light Snow": 14, "Snow": 15, "Light Freezing Fog":16, "": 999}
		self.list_station = ["Bend", "Eugene", "Seattle"]
		# Keys from Weather underground
		self.list_key_name = ["weather", "UV", "pressure_in", "dewpoint_f", "wind_mph", "temp_f"]
		self.wu_data = {}


	def import_data_wu(self, folder):
		try:
			os.stat(self.path_wu + folder)
		except:
			print("Cannot access the folder %s\n" % self.self.path_wu+folder)
		list_file = os.listdir(self.path_wu + folder)
		dict_output = {}
		history = {}
		pattern = r"\d+"
		for filename in list_file:
			if platform.system() == 'Windows':
				fp = open(self.path_wu + folder + '\\' + filename)
			else:
				fp = open(self.path_wu + folder + '/' + filename)
			content = json.load(fp)
			dict_output[filename] = {}
			## Error catch##
			if "error" in content["response"].keys():
				print ("File %s with error: %s" % (filename, content["response"]["error"]["description"]))
				if content["response"]["error"]["description"] == "you must supply a key" or content["response"]["error"]["description"] == "this key does not exist":
					fp.close()
					if platform.system() =='Windows':
						os.remove(self.path_wu + folder + '\\' + filename)
					else:
						os.remove(self.path_wu + folder + '/' + filename)
					del dict_output[filename]
					print ("[Warning] File %s removed" % filename)
			else:
				file_info = filename.split('_')
				dict_output[filename]["state"] = file_info[0]
				dict_output[filename]["city"] = file_info[1]
				dict_output[filename]["month"] = str(self.dict_calendar[file_info[2]])
				dict_output[filename]["year"] = file_info[4]
				dict_output[filename]["hour"] = file_info[5]
				dict_output[filename]["minute"] = str(int(int(file_info[6]) / 15) * 15)
				if dict_output[filename]["minute"] == "0":
					dict_output[filename]["minute"] = "00"
				for key_name in self.list_key_name:
					try:
						if key_name == "weather":
							dict_output[filename][key_name] = self.weather_cat[content["current_observation"][key_name]]
						# else:
						#	  dict_output[filename][key_name] = content["current_observation"][key_name]
						else:
							if re.findall(pattern, str(content["current_observation"][key_name])):
								# if (content["current_observation"][key_name] == "--") or (content["current_observation"][key_name] == "None"):
								dict_output[filename][key_name] = content["current_observation"][key_name]
							else:
								# print content["current_observation"][key_name]
								dict_output[filename][key_name] = history["current_observation"][key_name]
								# if key_name =="pressure_in":
								# print content["current_observation"][key_name]
								# dict_output[filename][key_name] = history[key_name]

						dict_output[filename]["day"] = file_info[3]
					except Exception as e:
						print ("[Error] Error code %s, filename %s" % (e, filename))
						break
				fp.close()
				history = content
		return dict_output

	def access_data(self, year=None, month=None, day=None, hour=None, minute=None):
		list_result = []
		for key, d in self.wu_data.items():
			if (d["month"] == str(month) or month is None) and \
					(day is None or d["day"] == str(day)) and \
					(hour is None or d["hour"] == str(hour)) and \
					(minute is None or d["minute"] == str(minute)) and \
					(year is None or d["year"] == str(year)):
				list_result.append(d)
		return list_result




if __name__ == '__main__':
	a = wu_util("Bend")
	try:
		a.wu_data = a.import_data_wu(a.name)  ##data is a dictionary
	except IOError as e:
		print("Cannot access to the folder (%s does not exist)" % a.name)
	for item in a.access_data(None, "11", "13", None, None):
		print(item)
