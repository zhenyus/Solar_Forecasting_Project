from __future__ import print_function
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import re
import operator
from sklearn import preprocessing
from scipy import stats
import pylab
from statsmodels.iolib.foreign import *
import statsmodels.api as sm
import statsmodels.formula.api as smf
from statsmodels.iolib.table import (SimpleTable, default_txt_fmt)

class Final_project(object):
    def __init__(self):
        self.path = "..\\data\\output\\"
        # self.name = ["Eugene", "Bend", "Seattle"]
        self.name =["Eugene"]
        self.list_key_name = ["weather", "UV", "pressure_in", "dewpoint_f", "wind_mph", "temp_f", "solar_radiation" , "beam_radiation" ,"diffuse_radiation"]

    def load_file(self, name):
        f = open(self.path + name)
        content = f.readlines()
        header = content.pop(0).strip().split("\t")
        data = []
        for line in content:
            string = [float(x) for x in line.strip().split("\t")]
            if string[0] != 999:  ##outlier
                data.append(string)
                # data.append(line.strip().split("\t"))
        return header, preprocessing.scale(np.asmatrix(data))

    def pca(self, data):
        pca = PCA(n_components=data.shape[1])
        pca.fit(data)
        return pca

    def graph_distribution(self, data, name, header):
        dict_data = {}
        # print data.shape
        for idx, value in enumerate(np.ndarray.tolist(np.asmatrix(data).T)[0]):
            # print value
            if value in dict_data:
                dict_data[value] +=1
            else:
                dict_data[value] = 1
        # print dict_data
        max_key = max(dict_data.iteritems(), key=operator.itemgetter(0))[0]
        min_key = min(dict_data.iteritems(), key=operator.itemgetter(0))[0]
        height = (dict_data[max(dict_data.iteritems(), key=operator.itemgetter(1))[0]] /100 +2 )*100
        # print data.shape
        n, bins, patches = plt.hist(data, 50, facecolor='green', alpha=0.75)
        plt.xlabel('Value')
        plt.ylabel('Amount')
        plt.title('For city %s, column %s' % (name, header))
        plt.axis([min_key-1, max_key+1, 0, height])
        plt.grid(True)
        plt.show()

    def graph_quantile_quantile(self, data, city, name):
        stats.probplot(data, dist="norm", plot=pylab)
        pylab.title("City: %s. Feature: %s" % (city, name))
        # pylab.savefig('QQ_%s_%s' % (city, name))
        # pylab.clf()
        pylab.show()



    def quantile_regression(self):
        pass

    def t_test(self):
        pass

    def get_output(self, header, dataset, name):
        f = open(self.path + name, 'w')
        f.write("\t".join(header)+'\n')
        for data in dataset:
            for value in data:
                f.write(str(value)+'\t')
            f.write('\n')
        f.close()
if __name__ == "__main__":
    a = Final_project()
    # col = 3
    for i in a.name:
        header, data = a.load_file(i)
        train_data = data[:, :-3]
        target_data = data[:, -3:]
        a.get_output(a.list_key_name, data.tolist(), "%s_test" % i)
        # tabl = SimpleTable(data.tolist(), header, [i for i in xrange(data.shape[0])], txt_fmt=default_txt_fmt)
        # print tabl
        # savetxt('test.out', train_data, delimiter=',')
        a.graph_distribution(data[:, -3], i, a.list_key_name[-3] )
        # print data.shape[1]
        # for col in range(data.shape[1]):
        #     # print col
        #     a.graph_quantile_quantile(data[:, col], i, a.list_key_name[col])
        print ("######################################################")
        # print result.components_
        # entropy, t-test, visualization, correlation, day shift hard-coded in uo_util
    # data = sm.datasets.engel.load_pandas().data
    # data.head()

    # print type(data)
    # mod = smf.quantreg('foodexp ~ income', data.tolist())
    # res = mod.fit(q=.5)
    # print(res.summary())

    #Sunrise sunsetKDAL