from __future__ import print_function
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import platform
import json
import os
import re
import sys
import math
import copy
#This file is for filling the missing/error data in wu_history new data

class data_filling_wu(object):
	def __init__(self, city, element_list):
		if platform.system() == 'Windows':
			self.split = "\\"
		else:
			self.split = "/"
		self.city = city
		self.sourceDirectory = "..%sdata%swu_history%s%s%snew%s"% (self.split, self.split, self.split, self.city, self.split, self.split)
		self.targetDirectory =  "..%sdata%swu_history%s%s%sprocessed%s"% (self.split, self.split, self.split, self.city, self.split, self.split)
		self.retrieve_list = element_list
		self.resolution = 60

	def read_json_file(self, filename):
		directory = self.sourceDirectory + filename
		with open(directory, 'r') as f:
			content = f.readlines()
		contents = "" 
		for row in content:
			contents += "".join(row.strip())
		jsonFile = json.loads(contents)
		key_list = copy.copy(self.retrieve_list)
		output = []
		for item in jsonFile:
			temp_dict = {}
			for key in key_list:
				temp_dict[key] = item[key]
			output.append(temp_dict)
			key_list = copy.copy(self.retrieve_list)
		return output

	def get_timespan(self, jsonFile):
		try:
			return(int(60 / (math.ceil(len(jsonFile[0]["Time"])/24.0))))
		except:
			if "TimePST" in jsonFile[0]:
				return(int(60 / (math.ceil(len(jsonFile[0]["TimePST"])/24.0))))
			else:
				return(int(60 / (math.ceil(len(jsonFile[0]["TimePDT"])/24.0))))
	

	def time_process(self, jsonfile):
		for item in jsonfile:
			time_list = []
			if "Time" in item:
				for time in item["Time"]:
					temp_list = time.strip().split(' ')
					if temp_list[1] == "AM":
						offset = 0
					else:
						offset = 12
					temp_time = temp_list[0].split(':')
					temp_time[0] = int(temp_time[0]) % 12 + offset
					temp_time[1] = int(temp_time[1])
					time_list.append(temp_time[0]*60+temp_time[1])
				item["Time"] = time_list
			#	item.pop("Time",None)
			else:
				for time in item["TimePDT"]:
					temp_list = time.strip().split(' ')
					if temp_list[1] == "AM":
						offset = 0
					else:
						offset = 12
					temp_time = temp_list[0].split(':')
					temp_time[0] = int(temp_time[0]) % 12 + offset
					temp_time[1] = int(temp_time[1])
					time_list.append(temp_time[0]*60+temp_time[1])
				item["Time"] = time_list
				item.pop("TimePDT",None)

	def filling_process(self, jsonfile):
		timespan = self.get_timespan(jsonfile)
		target_jsonfile = []
		for i, item in enumerate(jsonfile):
			control_list = [0] * int(1440 / timespan)
			push_index = []
			temp_dict = {}
			#Edit wrong data
##			for index, value in enumerate(item["DewPoint"]):
##				value = value.replace("%","")
##				if value == "-" or "N/A":
##					item["DewPoint"][index] = 0
##					try:
##						item["DewPoint"][index] = item["DewPoint"][index-1]
##					except:
##						try:
##							item["DewPoint"][index] = item["DewPoint"][index+1]
##						except:
##							pass
##				elif float(value)<0:
##					item["DewPoint"][index] = 0
##			for index, value in enumerate(item["Temperature"]):
##				if value == "-" or "N/A":
##					item["Temperature"][index] = 32
##					try:
##						item["Temperature"][index] = item["Temperature"][index-1]
##					except:
##						try:
##							item["Temperature"][index] = item["Temperature"][index+1]
##						except:
##							pass
##				elif float(value)<0:
##					item["Temperature"][index] = 0
			for index, value in enumerate(item["DewPoint"]):
				if value == '-':
					try:
						item["DewPoint"][index] = item["DewPoint"][index-1]
					except:
						item["DewPoint"][index] = item["DewPoint"][index+1]
				if value.find('%') != -1:
					item["DewPoint"][index] = item["DewPoint"][index][:-1]
					if item["DewPoint"][index] == "N/A":
						try:
							item["DewPoint"][index] = item["DewPoint"][index-1]
						except:
							item["DewPoint"][index] = item["DewPoint"][index+1]
			for index, value in enumerate(item["WindSpeed"]):
				try:
					if float(value) < 0 :
						item["WindSpeed"][index] = 0						
				except:
					item["WindSpeed"][index] = 0
				
			for key in item.keys():
				if key == "Time":
					temp_list = item[key]
					for index in push_index:
						try:
							time = item[key][index] - timespan
						except:
							time = item[key][index-1] + timespan
						temp_list.insert(index, time)
				else:
					temp_list = item[key]
					for index in push_index:
						try:
							if index == 0:
								pre = item[key][index]
								post = item[key][index]
							elif index == len(item[key]):
								pre = item[key][index-1]
								post = item[key][index-1]
							else:
								pre = item[key][index-1]
								post = item[key][index]
						except:
							pre = 0
							post = 0
						try:
							#insert numeric data
							temp_list.insert(index, (float(pre)+float(post)) / 2)
						except:
							#insert text data
							temp_list.insert(index, pre)
				temp_dict[key] = temp_list	
			target_jsonfile.append(temp_dict)
		return target_jsonfile	
	
	def export_json(self, jsonfile, filename):
		if os.path.exists(self.targetDirectory) is False:
			os.mkdir(self.targetDirectory)
		with open(self.targetDirectory + filename, 'w') as f:
			f.write(json.dumps(jsonfile))

	def export_csv(self, jsonfile, filename):
		if os.path.exists(self.targetDirectory) is False:
			os.mkdir(self.targetDirectory)
		with open(self.targetDirectory + filename, 'w') as f:
			pass	

	def get_better_timelist(self, jsonfile, element_list):
		# It must be done, takes O(n^2). To get a good format for merge
		newJsonFile = []
		history = []
		time = 1440
		formatTime = [int(i*self.resolution) for i in range(int(time/self.resolution))]
		for row in jsonfile:	
			jsonDict = {}
			timelist = row['Time']
			indexlist = self.find_map(timelist, formatTime)
			for index, element in enumerate(element_list):
				if element == 'Time':
					jsonDict[element] = formatTime
				elif element == 'Conditions':
					if(len(row[element]) ==0):
#						print(history)
						jsonDict[element] = [row['Events'][a] for a in indexlist]
					else:
						jsonDict[element] = [row[element][a] for a in indexlist]
#						history = row[element]
				else:
					jsonDict[element] = [row[element][a] for a in indexlist]
			newJsonFile.append(jsonDict)
		return newJsonFile
	def find_map(self,timelist, formatTime):
		indexlist = []
		for time in formatTime:
			minimum = 1440
			for time2 in timelist:
				v = abs(time-time2)
				minimum = min(minimum, v)
			try:
				indexlist.append(timelist.index(minimum+time))
			except:
				indexlist.append(timelist.index(time- minimum))
		return indexlist		
if __name__ == "__main__":
	SplitFlag = True
	try:	
		year = sys.argv[2]
		nameCity = sys.argv[1]
	except IndexError:
		year = "2016"
		nameCity = "Eugene"
		print("[Error] Format: python script City Year")

	element_list = ["Time", "DewPoint", "WindSpeed", "Temperature", "Conditions", "Events"]
	print("%s data filling..."% nameCity)
	a = data_filling_wu(nameCity, element_list)
	filename = "%s.json" %(year)
	print("Accessing File %s" % filename)
	jsonFile = a.read_json_file(filename)
	a.time_process(jsonFile)
	jsonFile_new = a.filling_process(jsonFile) #There is some bug in this method
	# Let us get a better time list!
	jsonFile_new = a.get_better_timelist(jsonFile, element_list)
	if(SplitFlag):
		print("Data file split into monthly files...")
		MonthRange = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365]
		if (len(jsonFile_new) == 366):
			print("Leap Year, Remove leap day!")
			jsonFile_new.remove(60)
		for i in range(1, 13):
			filename = "%s-%s.json"%(year, i)
			a.export_json(jsonFile_new[MonthRange[i-1]:MonthRange[i]], filename)
	else:
		a.export_json(jsonFile_new, filename)
#	print(jsonFile[0]["Time"])
#	print(jsonFile[0].keys())
