import json

class data_wu_parser(object):
	def __init__(self):
		self.title = ['Time ', 'Temp.', 'Windchill', 'Dew Point', 'Humidity', 'Pressure', 'Visibility', 'Wind Dir', 'Wind Speed', 'Gust Speed', 'Precip', 'Events', 'Conditions']
		pass

	def read_json(self, directory):
		with open(directory, 'r') as f:
			content = "".join(f.readlines())
		return json.loads(content)

if __name__ == '__main__':
	a = data_wu_parser()
	jsonfile = a.read_json("/home/campus19/zhenyus/Documents/Research/Solar_Forecasting_Project/data/wu_history/Eugene/new/items.json")
	title_index = []
	for index, row in enumerate(jsonfile):
		if row['content'] == a.title:
			title_index.append(index)

	print(len(title_index))
