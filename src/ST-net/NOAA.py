import os
import pandas

pathDataset = 'E:\\Research\\Solar_Forecasting_Project\\data\\NOAA'

# This is the whole 10 stations
# listStation = ['Bend', 'Boise', 'Burns', 'Challis', 'Dillon', 'Eugene', 'Hermiston', 'Moab', 'Seattle', 'Twin Falls']

# Debug use
listStation = ['Bend']

for station in listStation:
	path = os.path.join(pathDataset, station)
	dataframe = pandas.read_csv(os.path.join(path,os.listdir(path)[0]))
	print(dataframe.index)
