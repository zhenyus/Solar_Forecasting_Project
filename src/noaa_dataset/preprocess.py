#!/usr/bin/python
import sys,os
import numpy,pandas
import re

dataset_name = 'portland_hourly_2010.csv'

df = pandas.read_csv(dataset_name)
print(df.columns)


def date_parse(date_string):
	# '01-01T01:00:00' Format becomes month, day, hour, min, sec
	num = re.findall(r'\d\d',date_string)
	return num

date_parse('01-01T01:00:00')