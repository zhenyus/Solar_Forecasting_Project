from __future__ import print_function
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import platform
import sys
import os
import re

##This file is for preprocessing the data from UO SRML

class data_preprocess(object):
	def __init__(self, city, year= "2016"):
	# year not using right now
		self.city = city
		self.year = year
		#self.filetype = filetype
		self.city_dict = {"Eugene":"EUPO", "Seattle":"SEPQ", "Bend":"BERQ"}
		self.data = []
		if platform.system() == 'Windows':
			self.split = "\\"	
		else:
			self.split = "/" 
		self.directory ="..%sdata%suoregon%s%s" %( self.split, self.split, self.split, self.city)
		## Read all the data in the folder, data[index][0] is header, data[index][1] is list of data
		for filename in os.listdir(self.directory):
			try:
				self.data.append(self.preprocess_data_file(filename))
			except:
				print("[Warning] %s is not a file, skipped\n" % filename)	
	def read_data_file(self, filename):
		data = []
		directory = self.directory + self.split + filename
		with open(directory, 'r') as f:
			content = f.readlines() 
		header = [str(i) for i in content[0].strip().split('\t')]
		for i in range(len(content)-1):
			if content[i+1] != '\n':
				input = [str(i) for i in content[i+1].strip().split('\t')]
				data.append(input)
		return (header, data)
		
	def preprocess_data_file(self, filename):
	# This module is for wash the data by flags
		header, data = self.read_data_file(filename)
		flag_index_list = []
		history_data = ["0"]*len(header)
		for index, value in enumerate(header):
			if value == '0':
				flag_index_list.append(index)
		for index, item in enumerate(data):
			for i in flag_index_list:
				if item[i] == "99" or item[i] == "13":
					# data missing, try to retrieve next data, to take the average 
					try:
						nextdata = data[index+1][i-1]
					except:
						nextdata = history_data[i-1]
					item[i-1] = str((float(history_data[i-1]) + float(nextdata)) / 2.0)	
			history_data = item
		self.data_output(filename, header, data)
		return (header,data)

	def parse_flag(self, string):
	# This module is to handle the fla value
		pass
	
	def data_output(self, filename, header, data, filetype = 'txt'):
		print("Writing File %s to %s\n" % (filename.split('.')[0], filetype))
		directory = self.directory+self.split+"preprocessed"
		if os.path.exists(directory) is False:
			os.mkdir(directory)
		with open(("%s%s%s.%s"% (directory, self.split, filename.split('.')[0], filetype)),'w') as f:
			f.writelines("\t".join(header)+'\n')
			for i in range(len(data)):
				f.writelines("\t".join(data[i])+'\n')

if __name__ == '__main__':
	if len(sys.argv) >= 3:
		a = data_preprocess(sys.argv[1], sys.argv[2])
	elif len(sys.argv) == 2:
		a = data_preprocess(sys.argv[1])
	else:
		print("Input Argument Missed. Format is: python data_preprocess_uo.py City Year")
	
