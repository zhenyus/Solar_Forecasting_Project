from __future__ import print_function
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import platform
import sys
import os
import re
import json
import operator
import numpy as np
import pandas
##This file is for processing time data to circular normalized value

class data_circular_norm(object):
	def __init__(self, city, year="2016"):
		if platform.system() == 'Windows':
			self.split = "\\"	
		else:
			self.split = "/" 

		self.city = city
		self.year = year
		self.sourceDirectory = "..%sdata%smerged_data%s%s%s" % (self.split, self.split, self.split, self.city, self.split)
		self.targetDirectory = "..%sdata%scircular_norm_data%s%s%s" % (self.split, self.split, self.split, self.city, self.split)

#	def import_txt(self, filename):	
#		with open(self.uoDirectory+filename, 'r') as f:
#			content = f.readlines()
#		header = content.pop(0).strip().split('\t')
#		data = []
#		for row in content:
#			data.append(row.strip().split('\t'))
#		return header, data

	def export_txt(self, filename, data):
		if os.path.exists(self.targetDirectory) is False:
			os.mkdir(self.targetDirectory)
		with open(self.targetDirectory + filename, 'w') as f:
			header = ['sin_day','cos_day', 'sin_time', 'cos_time', '1', '2', '3', 'DewPoint', 'WindSpeed', 'Temperature', 'Conditions']
			f.write("\t".join(header)+'\n')
			for row in data:
				f.write("\t".join(map(str, row))+ '\n')

	def circular_norm(self, array, period = 360.0):
		sinCol = np.zeros((array.shape[0]))
		cosCol = np.zeros((array.shape[0]))
		array = array*float(period)/360.0
		for i in range(array.shape[0]):
			sinCol[i] = np.sin(array[i])
			cosCol[i] = np.sin(array[i])
		return sinCol, cosCol 


if __name__ == '__main__':
	## City
	a = data_circular_norm(sys.argv[1])

	for filename in os.listdir(a.sourceDirectory):
		dataframe = pandas.read_csv(a.sourceDirectory + filename, sep='\t')
		#data = np.genfromtxt(a.sourceDirectory + filename, skip_header=1)
		data = dataframe.values
		sin_day, cos_day = a.circular_norm(data[:,0], period=365)
		sin_time, cos_time = a.circular_norm(data[:,1]/100, period=24.0)
		outputData = np.column_stack((sin_day, cos_day, sin_time, cos_time, data[:,2:]))
		a.export_txt(filename, np.ndarray.tolist(outputData))
