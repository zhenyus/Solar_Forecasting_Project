from __future__ import print_function
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import platform
import json
import os
import re
import sys
import math
import copy
#This file is for filling the missing/error data in wu_history monthly data

class data_filling_wu(object):
	def __init__(self, city, element_list):
		if platform.system() == 'Windows':
			self.split = "\\"
		else:
			self.split = "/"
		self.city = city
		self.sourceDirectory = "..%sdata%swu_history%s%s%smonthly data%s"% (self.split, self.split, self.split, self.city, self.split, self.split)
		self.targetDirectory =  "..%sdata%swu_history%s%s%sprocessed%s"% (self.split, self.split, self.split, self.city, self.split, self.split)

		self.retrieve_list = element_list

	def read_json_file(self, filename):
		directory = self.sourceDirectory + filename
		with open(directory, 'r') as f:
			content = f.readline().strip()
		jsonFile = json.loads(content)
		key_list = copy.copy(self.retrieve_list)
		output = []
		for item in jsonFile:
			if "TimePST" in item:
				key_list.append("TimePST")
			else:
				key_list.append("TimePDT")
			temp_dict = {}
			for key in key_list:
				temp_dict[key] = item[key]
			output.append(temp_dict)
			key_list = copy.copy(self.retrieve_list)
		return output

	def get_timespan(self, jsonFile):
		try:
			return(int(60 / (math.ceil(len(jsonFile[0]["Time"])/24.0))))
		except:
			if "TimePST" in jsonFile[0]:
				return(int(60 / (math.ceil(len(jsonFile[0]["TimePST"])/24.0))))
			else:
				return(int(60 / (math.ceil(len(jsonFile[0]["TimePDT"])/24.0))))
	

	def time_process(self, jsonfile):
		for item in jsonfile:
			time_list = []
			if "TimePST" in item:
				for time in item["TimePST"]:
					temp_list = time.strip().split(' ')
					if temp_list[1] == "AM":
						offset = 0
					else:
						offset = 12
					temp_time = temp_list[0].split(':')
					temp_time[0] = int(temp_time[0]) % 12 + offset
					temp_time[1] = int(temp_time[1])
					time_list.append(temp_time[0]*60+temp_time[1])
				item["Time"] = time_list
				item.pop("TimePST",None)
			else:
				for time in item["TimePDT"]:
					temp_list = time.strip().split(' ')
					if temp_list[1] == "AM":
						offset = 0
					else:
						offset = 12
					temp_time = temp_list[0].split(':')
					temp_time[0] = int(temp_time[0]) % 12 + offset
					temp_time[1] = int(temp_time[1])
					time_list.append(temp_time[0]*60+temp_time[1])
				item["Time"] = time_list
				item.pop("TimePDT",None)

	def filling_process(self, jsonfile):
		timespan = self.get_timespan(jsonfile)
		target_jsonfile = []
		for i, item in enumerate(jsonfile):
			control_list = [0] * int(1440 / timespan)
			push_index = []
			temp_dict = {}
			#Edit wrong data
			for index, value in enumerate(item["Dew PointF"]):
				if float(value)<0:
					item["Dew PointF"][index] = 0
			for index, value in enumerate(item["TemperatureF"]):
				if float(value)<0:
					item["TemperatureF"][index] = 0
			for index, value in enumerate(item["Wind SpeedMPH"]):
				if value == 'Calm':
					item["Wind SpeedMPH"][index] = 0
				elif float(value) < 0 :
					item["Wind SpeedMPH"][index] = 0
			for value in item["Time"]:
				control_list[int(value/timespan)] = 1   ##0 means filling required
			#Insert missing data
			for index, value in enumerate(control_list):
				if value == 0:
					push_index.append(index)
			for key in item.keys():
				if key == "Time":
					temp_list = item[key]
					for index in push_index:
						try:
							time = item[key][index] - timespan
						except:
							time = item[key][index-1] + timespan
						temp_list.insert(index, time)
				else:
					temp_list = item[key]
					for index in push_index:
						if index == 0:
							pre = item[key][index]
							post = item[key][index]
						elif index == len(item[key]):
							pre = item[key][index-1]
							post = item[key][index-1]
						else:
							pre = item[key][index-1]
							post = item[key][index]
						try:
							#insert numeric data
							temp_list.insert(index, (float(pre)+float(post)) / 2)
						except:
							#insert text data
							temp_list.insert(index, pre)
				temp_dict[key] = temp_list	
			target_jsonfile.append(temp_dict)
		return target_jsonfile	
	
	def export_json(self, jsonfile, filename):
		if os.path.exists(self.targetDirectory) is False:
			os.mkdir(self.targetDirectory)
		with open(self.targetDirectory + filename, 'w') as f:
			f.write(json.dumps(jsonfile))

	def export_csv(self, jsonfile, filename):
		if os.path.exists(self.targetDirectory) is False:
			os.mkdir(self.targetDirectory)
		with open(self.targetDirectory + filename, 'w') as f:
			pass	
					
if __name__ == "__main__":
	year = "2016"
	nameCity = sys.argv[1]
	element_list = ["Conditions", "Dew PointF", "Wind SpeedMPH", "TemperatureF"]
	print("%s data filling..."% nameCity)
	a = data_filling_wu(nameCity, element_list)
	for i in range(1,13):
		filename = "%s-%s.json" %(year, i)
		print("Accessing File %s" % filename)
		jsonFile = a.read_json_file(filename)
		a.time_process(jsonFile)
		jsonFile_new = a.filling_process(jsonFile)
		a.export_json(jsonFile_new, filename)
	
#	print(jsonFile[0]["Time"])
#	print(jsonFile[0].keys())
