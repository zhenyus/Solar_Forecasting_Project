from __future__ import print_function
#!/usr/bin/env python
# -*- coding: utf-8 -*-

## This class is for University of Oregon Solar Radiation Monitoring Laboratory file resolution ##

import os
import sys
import csv
import json
import re
import platform

RESOLUTION = 15  # data resolution


class uo_util(object):
	def __init__(self, city, year="2016"):
		if platform.system() == 'Windows':
			self.split = "\\"
			self.path_uo = "..\\data\\uoregon\\"
			self.path_station_code = os.getcwd() + '\\dict\\station_code.csv'
		else:
			self.split = "/"
			self.path_uo = "../data/uoregon/"
			self.path_station_code = os.getcwd() + '/dict/station_code.csv'
		# Hardcoded station name
		self.city = city
		self.list_station = ["Bend", "Eugene", "Seattle"]
		self.dict_header = {}
		self.dict_station_code = {}
		self.dict_day_scan = {1: 0, 2: 31, 3: 60, 4: 91, 5: 121, 6: 152, 7: 182, 8: 213, 9: 244, 10: 274, 11: 305, 12: 335}
		self.sourceDirectory = "..%sdata%suoregon%s%s%spreprocessed%s"% (self.split, self.split, self.split,self.city,self.split, self.split)
		self.targetDirectory = "..%sdata%suoregon%s%s%sretrived%s"% (self.split, self.split, self.split,self.city, self.split, self.split)
		if(city == "Eugene"):
			self.codelist = ['1000', '2010', '3001']	
		elif(city == "Bend"):
			self.codelist = ['1001', '2011', '3001']
		elif(city == "Seattle"):
			self.codelist = ['1000', '2010', '3001']
		# Load station code dictionary
		
		with open(self.path_station_code, 'rt') as csvfile:
			spamreader = csv.reader(csvfile, delimiter='\t', quotechar='|')
			for row in spamreader:
				self.dict_station_code[row[0]] = {"location": row[1], "abbr": row[2]}
				#  Data element numbers

	def import_data_uo(self, folder):
		try:
			os.stat(self.path_uo + folder)
		except WindowsError as e:
			print ("[Warn] Cannot locate the folder. Create a new one instead.")
			os.mkdir(self.path_uo + folder)
		list_file = os.listdir(self.path_uo + folder)
		list_output = []
		dict_format = {}
		for filename in list_file:
			if platform.system() == 'Windows':
				fp = open(self.path_uo + folder + '\\' + filename)
			else:
				fp = open(self.path_uo + folder + '/' + filename)
			content = fp.readlines()
			header = content.pop(0).strip().split('\t')
			## station, year, code1, flag1, code2, flag2...##
			station = header.pop(0)
			year = header.pop(0)
			# dict_format[station]= {"year": year, "data":[]}
			# dict_item = {}
			# for idx, item in enumerate(header):
			#	  if idx % 2 == 0:
			#		  dict_item["type"] = item
			#	  else:
			#		  dict_item["flag"] = item
			#		  dict_format[station]["data"].append(dict_item)
			#		  dict_item = {}
			for line in content:
				list_line = line.strip().split('\t')
				dict_out = {"day": list_line.pop(0), "time": list_line.pop(0), "data": []}
				dict_item = {}
				for idx, item in enumerate(list_line):
					if idx % 2 == 0:
						dict_item[header[idx]] = item
					else:
						dict_item[header[idx]] = item
						dict_out["data"].append(dict_item)
						dict_item = {}
				if dict_out != {}:
					list_output.append(dict_out)
		return list_output

	def get_column(self, data, code):
		list_output = []
		for idx, list_item in enumerate(data):
			output = []
			for item in list_item["data"]:
				for c in code:
					if c in item:
						i = item
						output.append(item)
				list_output.append(output)
		return list_output

	## Ignore the flag control
	def fast_access(self, folder, filename, list_code):
		path = self.path_uo+folder+self.split+'preprocessed'+self.split+filename
		history = {}
		list_file = os.listdir(self.path_uo + folder)
		print("Accessing file %s"%path)
		fp = open(path, 'rt')
		content = fp.readlines()
		# get the header
		header = content.pop(0).split('\t')
		# split the data for each line string to list
		for idx, line in enumerate(content):
			content[idx] = line.strip().split('\t')
		content = [line for line in content if line != ['']]
		list_idx = []
		# filter the data required by list_code given
		for idx, item in enumerate(header):
			for code in list_code:
				if item == code:
					list_idx.append(idx)
		list_output = []

		for line in content:
			if int(line[1].rjust(4, '0')[-2:]) % RESOLUTION == 0:	# determine if the time string has 0 on minutes part
				output = []
				for idx in list_idx:
				#we ignore the flags because it should be handled already in data_preprocess
					output.append(line[idx])	
					#if line[idx + 1] != "99" and line[idx + 1] != "13":
					#	output.append(line[idx])
					#else:
					# We use zero
					#	output.append("0")
				list_output.append(({"day": str(int(line[0])-self.dict_day_scan[int(filename[6:8])]), "time": '{:04}'.format(int(line[1]))}, output))
			history = line
		return list_output

	def data_retrieve(self, filename, listCode):
		listOutput = []
		filepath = self.sourceDirectory + filename
		print("Retrieve data from %s"% filepath)
		with open(filepath, 'r') as f:
			content = f.readlines()
		header = content.pop(0).strip().split('\t')
		headerOutput = ["day", "time"]
		data = [i.strip().split('\t') for i in content]
		retrieve_index = [0,1]
		for idx in listCode:
			try:
				retrieve_index.append(header.index(idx))
			except:
				retrieve_index.append(header.index(str(int(idx)+1)))
		headerOutput += [header[i] for i in retrieve_index[2:]]
		for row in data:
			listOutput.append([row[i] for i in retrieve_index])
		#self.export_data(filename, headerOutput, listOutput)
		return (headerOutput, listOutput)
			###need to be done before may 11 

	def export_data(self, filename, header, data, filetype = 'txt' ):
		directory = self.path_uo+self.city+self.split+'retrieved'
		if os.path.exists(directory) is False:
			os.mkdir(directory)
		print("Export data to %s%s%s"% (directory, self.split, filename))
		month = int(filename[filename.find('.')-2:filename.find('.')])
		for index, row in enumerate(data):
			data[index][0] = str(int(data[index][0]) - self.dict_day_scan[month])
		with open(("%s%s%s.%s"% (directory, self.split, filename.split('.')[0], filetype)),'w') as f:
			f.writelines("\t".join(header)+'\n')
			for i in range(len(data)):
				f.writelines("\t".join(data[i])+'\n')
		
			
	def export_data_r(self,data):		
		with open('t.csv','w') as f:
			f.writelines("\t".join(self.codelist)+'\n')
			for i in data:
				f.writelines("\t".join(i[1])+'\n')	

if __name__ == '__main__':
	a = uo_util(sys.argv[1])
	# bend = a.import_data_uo(a.list_station[0])
	# eugene = a.import_data_uo(a.list_station[1])
	# seattle = a.import_data_uo(a.list_station[2])

	# data_bend = a.get_column(bend, [ "1001", "2011","3001"])
	# data_eugene = a.get_column(eugene, [ "1000", "2010","3001"])
	# data_seattle = a.get_column(seattle, [ "1000", "2010","3001"])
	# print data_bend[0]
	# data_bend = a.fast_access("Bend", "BERQ1610.txt", ["1001", "2011", "3001"])
	#data_eugene = a.fast_access("Bend", "BERQ1611.txt", ["1001", "2011", "3001"])
	#print(data_eugene[0:30])
	#a.export_data(data_eugene)

	
	path = a.path_uo+a.city+a.split+'preprocessed'+a.split
	for filename in os.listdir(path):
		try:
			header, data = a.data_retrieve(filename, a.codelist)
			a.export_data(filename, header, data)
			print("%s has been processed." % filename)		
		except:
			print("error happens for file %s" % filename)
#	a.data_retrieve("BERQ1610.txt",["1001","2011","3001"])
