import scrapy
from suntime_crawler.items import SuntimeCrawlerItem


class SuntimeSpider(scrapy.Spider):
    name = "suntime"
    allowed_domain = "wunderground.com"
    url_samples = "https://www.wunderground.com/history/airport/KEUG/2016/12/25/DailyHistory.html?req_city=Eugene&req_state=OR"
    year = ["2016"]
    month = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    day = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    start_urls = []
    for i in year:
        for idx, j in enumerate(month):
            for k in range(1, day[idx] + 1):
                # Eugene
                # start_urls.append(
                #     "https://www.wunderground.com/history/airport/KEUG/%s/%s/%s/DailyHistory.html?req_city=Eugene&req_state=OR" % (
                #     i, j, k))
                # Bend
                # start_urls.append(
                #     "https://www.wunderground.com/history/airport/KBDN/%s/%s/%s/DailyHistory.html?req_city=Bend&req_state=OR" % (
                #     i, j, k))
                # Seattle
                # start_urls.append(
                #     "https://www.wunderground.com/history/airport/KBFI/%s/%s/%s/DailyHistory.html?req_city=Seattle&req_state=WA" % (
                #     i, j, k))

        def parse(self, response):
            xpath = "//*[@id=\"astronomy-mod\"]/table[1]/tbody[1]/tr[1]"
            for sel in response.xpath(xpath):
                item = SuntimeCrawlerItem()
                item["sunrise"] = sel.xpath('td[2]/text()').extract()
                item["sunset"] = sel.xpath('td[3]/text()').extract()
                yield item
                # //*[@id="astronomy-mod/"]/table[1]/tbody[1]/tr[3]
