from __future__ import print_function
import os
import platform

class wu_hist(object):
	def __init__(self, name):
		if platform.system() == 'Windows':
			self.path_wu_history = "..\\data\\wu_history\\"
		else:
			self.path_wu_history = "../data/wu_history/"
		self.name = name
		self.header = ["TimePST", "TemperatureF", "Dew PointF", "Humidity", "Sea Level PressureIn", "VisibilityMPH",
					   "Wind Direction",
					   "Wind SpeedMPH", "Gust SpeedMPH", "PrecipitationIn", "Events", "Conditions", "WindDirDegrees",
					   "DateUTC"]

	def access_history(self, folder):
		try:
			os.stat(self.path_wu_history + folder)
		except WindowsError as e:
			print("[Warn] Cannot locate the folder. Create a new one instead.")
			os.mkdir(self.path_wu_history + folder)
		list_output = []
		list_file = os.listdir(self.path_wu_history + folder)
		for filename in list_file:
			if platform.system() == 'Windows':
				fp = open(self.path_wu_history + folder + '\\' + filename)
			else:
				fp = open(self.path_wu_history + folder + '/' + filename)
			content = fp.readlines()
			fp.close()
			print(content.pop(0))
			content.pop(0)
			list_output.append([i.split(',') for i in content])
		return list_output

if __name__ == "__main__":
	a = wu_hist("Bend")
	b = a.access_history(a.name)
	print(b[0])
