from __future__ import print_function
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import platform
import json

#from uo_util import *
from data_preprocess_uo import *
## This class is for suntime scaling within each year

class suntime(object):
	def __init__(self, city, year= "2016"):
		self.city = city
		self.year = year
		if platform.system() =='Windows':
			self.split = '\\'
		else:
			self.split = '/'
		self.sourceDirectory = "..%sdata%ssuntime_history%s"% (self.split, self.split, self.split)
		self.sourceDirectoryUO = "..%sdata%suoregon%s%s%sretrieved%s"% (self.split, self.split, self.split, self.city, self.split, self.split)
		self.targetDirectoryUO = "..%sdata%suoregon%s%s%ssuntime_filter%s"% (self.split, self.split, self.split, self.city, self.split, self.split)

		self.file = self.read_json_file(city+'_'+str(year)+'.json')
		self.range = self.get_approx_range(self.get_most_suntime_index(), 15)
	
	def read_json_file(self, filename):
		return json.loads(open(self.sourceDirectory+ filename).read())

	def parse(self, data): ##String with the format ["HH:MM AM PST"] 
		return int(data[0].split(' ')[0].split(':')[0])*60+int(data[0].split(' ')[0].split(':')[1])

	def get_diff(self,index):
		return self.parse(self.file[index]['sunset']) - self.parse(self.file[index]['sunrise'])+720

	def get_most_suntime_index(self):
		diff_list = [self.get_diff(i) for i in range(len(self.file))]
		diff = max(diff_list)
		return diff_list.index(diff)

	def get_approx_range(self, index, div): ##div is the resolution
		sunrise = self.parse(self.file[index]['sunrise'])
		sunset = self.parse(self.file[index]['sunset'])
		return (sunrise-sunrise%div, sunset-sunset%div+div+720)
	
	def suntime_scale(self, uo_data, process=True):
		sunrise_hour = self.range[0]/60
		sunrise_min = self.range[0]%60
		sunset_hour = self.range[1]/60
		sunset_min = self.range[1]%60
		listFiltered = []
		listFilter = []
		#############################
		header = uo_data.pop(0).strip().split('\t')
		data = [i.strip().split('\t') for i in uo_data]
		if (process == False):
#			sunrise_hour = 0
#			sunrise_min = 0
#			sunset_hour = 23
#			sunset_min = 30
			return header, data
		for row in data:
			hour = int(int(row[1])/100)
			minute = int(row[1])%100
	#		print( hour , minute)
			if (hour < sunrise_hour):
				listFiltered.append(row)
			elif (hour > sunset_hour):
				listFiltered.append(row)
			elif (hour == sunset_hour and minute >= sunset_min):
				listFiltered.append(row)
			elif (hour == sunrise_hour and minute <= sunrise_min):
				listFiltered.append(row)
			else:
				listFilter.append(row)

		return header, listFilter ##listFiltered Not used

	def import_retrieve_file_uo(self, filename):
		directory = self.sourceDirectoryUO + filename

	def export_data(self, filename, header, data, filetype = 'txt' ):
		if os.path.exists(self.targetDirectoryUO) is False:
			os.mkdir(self.targetDirectoryUO)
		print("Export data to %s%s"% (self.targetDirectoryUO, filename)) 
		with open(("%s%s.%s"% (self.targetDirectoryUO, filename.split('.')[0], filetype)),'w') as f:
			f.writelines("\t".join(header)+'\n')
			for i in range(len(data)):
				f.writelines("\t".join(data[i])+'\n')
	
	def export_to_file(self, header, data):
		pass
if __name__ == '__main__':
	suntime = suntime(sys.argv[1])
	i = suntime.get_most_suntime_index()
	print(suntime.file[i]['sunset'], suntime.file[i]['sunrise'])
#	print(suntime.get_diff(i))
#	print(suntime.get_approx_range(i, 15))
	for filename in os.listdir(suntime.sourceDirectoryUO):
		content = open(suntime.sourceDirectoryUO+filename,'r').readlines()
		header, listFilterData = suntime.suntime_scale(content, process=False)
		suntime.export_data(filename, header, listFilterData)
